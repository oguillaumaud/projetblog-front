import type { Comment} from "@/entities";
import axios from "axios";

export async function fetchComment(id:number){
    const response = await axios.get<Comment[]>('/api/comment/'+id);
    return response.data;
}

export async function deleteOneComment(id:any){
    await axios.delete<void>('/api/comment/'+id);
}

export async function postComment(comment:Comment){
    const response = await axios.post<Comment>('/api/comment', comment);
    return response.data;
}