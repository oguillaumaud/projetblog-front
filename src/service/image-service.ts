import type { Image } from "@/entities";
import axios from "axios";

export async function fetchImage(id:number){
    const response = await axios.get<Image[]>('/api/image/'+id);
    return response.data;
}

export async function postImage(image:Image){
    const response = await axios.post<Image>('/api/article/', image);
    return response.data;
}