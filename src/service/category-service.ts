import type { Category } from "@/entities";
import axios from "axios";

export async function fetchCategory(){
    const response = await axios<Category[]>('/api/category');
    return response.data
}