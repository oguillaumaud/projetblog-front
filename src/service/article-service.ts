import type { Article } from "@/entities";
import axios from "axios";

export async function fetchArticle(){
    const response = await axios.get<Article[]>('/api/article');
    return response.data;
}

export async function fetchLastArticle(){
    const response = await axios.get<Article[]>('/api/article/latest');
    return response.data;
}

export async function fetchOneArticle(id:any){
    const response = await axios.get<Article>('/api/article/'+id);
    return response.data;
}

export async function postArticle(article:Article){
    const response = await axios.post<Article>('/api/article', article);
    return response.data;
}

export async function deleteArticle(id:any){
    await axios.delete<void>('/api/article/'+id);
}

export async function updateArticle(article:Article) {
    const response = await axios.put<Article>('/api/article/'+article.id, article)
    return response.data
}