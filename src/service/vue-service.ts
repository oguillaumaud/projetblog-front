import type { Vue } from "@/entities";
import axios from "axios";

export async function fetchVue(){
    const response = await axios.get<Vue[]>('/api/vue');
    return response.data;
}

export async function fetchOneVue(id:any){
    const response = await axios.get<Vue>('/api/vue/'+id);
    return response.data;
}

export async function postVue(vue:Vue){
    const response = await axios.post<Vue>('/api/vue', vue);
    return response.data;
}

export async function deleteVue(id:any){
    const response = await axios.delete<void>('/api/vue/'+id);
    return response.data;
}