export interface Category{
    id?: number
    name: string
}

export interface User{
    id?:number
    userName?: string
    email:string
    password?: string
    role?:string
}

export interface Article{
    id?: number
    title: string
    author?:string
    images: Image[]
    paragraph:string
    category: Category
    date: string
    vue: number
    owner?: User
}

export interface Vue{
    id?: number
    like: number
    dislike: number
    vue: number
}

export interface Comment{
    id?: number
    paragraph: string
    date?: string
    articleId: number
    owner?: User
}

export interface Image{
    id?: number
    src: string
    text?: string
}
