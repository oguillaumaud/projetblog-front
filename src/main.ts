import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'
import './axios-config';

import { createPinia } from 'pinia'

import { createVuetify } from 'vuetify'

import { aliases, mdi } from 'vuetify/iconsets/mdi'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'


const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'mdi',
        aliases,
        sets: {
            mdi,
        },
    },
})




createApp(App).use(vuetify).use(router).use(createPinia()).mount('#app')