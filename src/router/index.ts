import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ArticlesView from '@/views/ArticlesView.vue'
import SingleArticleView from '@/views/SingleArticleView.vue'
import AddArticleView from '@/views/AddArticleView.vue'
import ConnectView from '@/views/ConnectView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: HomeView
    },
    {
      path: '/article',
      component: ArticlesView
    },
    {
      path: '/article/:id',
      component: SingleArticleView
    },
    {
      path: '/add-article',
      component: AddArticleView
    },
    {
      path:'/login',
      component: ConnectView
    }
  ]
})

export default router
